var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/about', function(req, res, next) {
  res.render('about');
});

router.get('/questions', function(req, res, next) {
  res.render('questions');
});

router.get('/template', function(req, res, next) {
  res.render('template');
});
module.exports = router;