# NaturesPalette
Nature’s Palette: An open-access digital repository of spectral data

Introduction

Nature’s Palette is a online repository of spectral data.
This online repository is a prototype of an online repository requested by Dr. Paul Bitton.

Githlab URL
https://

RequiredSoftware
⦁	Node.js
⦁	MongoDB

Installation steps
To run the project locally:
⦁	Clone it.
⦁	Open models.js file and replace the mongoDB mongoose connection string.
⦁	Open command line terminal; change the path to project folder.
⦁	The main file for the project is app.js. To run this file, type the command -> node app.js in the command line terminal. The project runs at http://localhost:3331/
